package com.example.demont;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoNtApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoNtApplication.class, args);
	}

}
